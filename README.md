# Charge Exchange Search - Data Analysis with CFF

## Brief introduction

CFF is the "Commond FSQ Framework" and is basically a set of tools to
produce (flat, simple) ROOT trees in a unified way, exchange and share
them via grid tools, run python analysis on them in a standalone
environment (-> your laptop) using a parallel proof framework. Thus,
it is an excellent analysis framework and we have used it in FSQ for a
series of papers.

To set it up, you need to take a few steps: 
- get CFF from https://github.com/hvanhaev/CommonFSQFramework (```git clone https://github.com/hvanhaev/CommonFSQFramework.git```)
- switch to CFF branch, "CMSSW_80X", since 2016 data was taken with CMSSW_8XY releases (```git checkout CMSSW_80X ```)
- Select:
  - if you work inside CMSSW/src path, run ```scram b``` (Note: CMSS_8_0_33 on lxplus6 is tested and works)
  - if you work stand alone (w/o CMSSW), e.g. on your laptop, run ```makeStandalone.sh```
- edit MyAnalysis.py and adapt "TTreeBasePATH" to a local directory on your system with enough space to store the data files
- check setenv_ChargeExchange.sh and make sure that PYTHONPATH will point to the directory, that contains ```CommonFSQFramwork```, this is automatic if you follow the suggestion and put CommonFSQFramework and ChargeExchange-cff next to each other in the same directory. 
- done, you can use CFF now

## Dataset list

Currently this is 

- data_PARun2016D_ChargeExchange_HLTZeroBias
  - in preparation 
  - run 286520, PARun2016D/AOD
  - HLT_PAZeroBias skim

- data_PARun2016D_ChargeExchange_HLTRandom 
  - 118 files
  - run 286520, PARun2016D/AOD
  - HLT_PARandom skim

- data_ALICERun_2016
  - just CASTOR and PixeRecHits
  - Runs: 285143, 285144, 285146, 285147, 185149, PARun2016B-v1/RAW
  - 213 files
 
Dataset content is
- basic event data, lumi, beamspot
- CASTOR
- PF candidates
- reco tracks, vertices
- L1 data
- ZDC: missing ?? how to add ??

## Some background info

This is not essential for analysis work, but helps to understand how CFF works and how you do more fancy things.

CFF datasets are defined in ```setenv_ChargeExchange.sh``` in the variable
```SmallXAnaVersion=Samples_ChargeExchange```, which right now points
to ```Samples_ChargeExchange.py```. Inside this file you may create
new samples (triggers, datasets, etc.). Read CFF tutorials how to use
crab3 submission to produce new trees. Each sample has ```pathSE'''
which is where the data lives on the grid. With a grid certificate and
gfal-ls you can look at this, or copy it. In addition there is
```pathTrees``` which is where the file may be copied locally on your
system. Note that ```@CFF_LOCALTreeDir@``` is replace by
```TTreeBasePATH``` from ```MyAnalysis.py```.

### Create new CFF dataset

- create new dataset section in SampleList. Important are the entries
  "name", "isData", "json" if you want to give a json file, and "DS",
  the content of the other entries is relevant only later.

- prepare a json file, if you need

- edit the ```crabcfg_``` file, this is a standard crab-cfg file, but
  some of the lines will be added by CFF and changed
  automatically. Use the example as a template for a new file. The
  most important line is ```psetName``` to point to the actual
  ```treemaker.py``` script.

- edit the ```treemaker.py``` script. Treemaker has to be configured
  properly for RAW/RECO/AOD etc. reading (->unpacker, rawtodigi
  etc.). You may add L1 or HLT filters, You have to import
  ```CommonFSQFramework.Core.customizePAT``` and use the
  ```CFFTreeProducer``` with ```Views```. The Views are defined in
  CFF/Core/src and read CMSSW events to convert them into plain ROOT
  tree entries. 

- start production with the ```runCrab3Jobs.py``` helper script.


### Copying CFF data files locally (-> copy one CFF dataset)

While you may run analysis on data that is stored remotely on the
grid, it is much better to copy it locally first. For this, first set
```TTreeBasePATH``` from ```MyAnalysis.py``` to a local directory with
enough space. 

Then you may run the ```copyAnaData.py -t``` tool to do the
copying. Note, this requires grid access and a valid
certificate. Typically this only works on LXPLUS. 

Another note:

All data is also stored on our KIT filesystem. We can copy it from there to any location directly, too. 


## Run analysis

run ```printTTree.py``` to get a list of all available datasets. 

run ```printTTree.py sample_name``` to get a detailed list of content variables stored in one dataset

make a copy of ```TemplateAnalysis.py``` with a suited name for your task. Start the analysis with ```python TemplateAnaysis.py sample_name```

You may edit the ```run_all``` call at the bottom of
```TemplateAnalysis.py``` in order to change the number of parallel
jobs, and also the number of maximum events you want to analyse. 

The output is saved in a ROOT file at './'

